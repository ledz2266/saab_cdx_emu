# CSR8645 adapter for SAAB 9-5 and 9-3
*This project bulds upon the [BlueSaab](https://github.com/bholmin/SAAB-CDC)*

Bluetooth module that emulates Pioneer CD Changer.

## Hardware used:
* STM32F303K6T - Microcontorller
* CSR8645 - Bluetooth module
* SN65HVD230 - CAN transceiver
* PESD1CAN - CAN bus protection diode
