/*
 * UART.h
 *
 *  Created on: 11 Feb 2019
 *      Author: dels
 */

#ifndef UART_H_
#define UART_H_

#include "main.h"
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

extern UART_HandleTypeDef huart2;

void huart2_send(uint8_t TxBuffer[], uint8_t len);
void huart2_send_byte(uint8_t TxByte);
void huart2_send_char(char TxChar);
void huart2_send_CANID(uint32_t TxBytes);
void huart2_send_CANDATA(uint8_t TxBuffer[8]);

#endif /* UART_H_ */
