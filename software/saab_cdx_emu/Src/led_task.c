/*
 * led_task.c
 *
 *  Created on: 8 Feb 2019
 *      Author: dels
 */
#include "led_task.h"

void SetDebugLed(void){
  osThreadResume(debugLedTaskHandle);
}

void StartDebugLedTask(void const * argument){
  uint32_t count;
  for (;;){
    count = osKernelSysTick() + 500;
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, 1);

    while(count > osKernelSysTick()){
      osDelay(200);
    }

    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, 0);
    osThreadSuspend(NULL);
  }

}
