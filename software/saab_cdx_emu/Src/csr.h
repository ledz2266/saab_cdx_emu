/*
 * csr.h
 *
 *  Created on: Apr 27, 2019
 *      Author: delusion
 */

#ifndef CSR_H_
#define CSR_H_

#include <stdbool.h>

#define CSR_SHORT_PRESS 150
#define CSR_LONG_PRESS 1000
#define CONFIRMATION_SOUND true

void NextTrack(void);
void PrevTrack(void);
void PlayPause(void);
void BtVisible(bool visibility);
void Power(bool state);

#endif /* CSR_H_ */
