/*
 * csr.c
 *
 *  Created on: Apr 27, 2019
 *      Author: delusion
 */
#include "csr.h"
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"


void NextTrack(void){
  if (CONFIRMATION_SOUND){
    Beep(0x08);
  }
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 1);
  osDelay(CSR_SHORT_PRESS);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 0);
}

void PrevTrack(void){
  if (CONFIRMATION_SOUND){
    Beep(0x10);
  }
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 1);
  osDelay(CSR_SHORT_PRESS);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, 0);
}

void PlayPause(void){
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 1);
  osDelay(CSR_SHORT_PRESS);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
}

void BtVisible(bool visibility){
  if (visibility){
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 1);
  }  else{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, 0);
  }
}

void Power(bool state){
  if (state){
    if (CONFIRMATION_SOUND){
      Beep(0x04);
    }
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 1);
  } else{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, 0);
  }

}
