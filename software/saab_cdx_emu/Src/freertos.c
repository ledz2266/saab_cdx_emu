/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include <stdbool.h>
#include "led_task.h"
#include "can.h"
#include "CDC.h"
#include "UART.h"
#include "csr.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
bool cdcActive = false;
bool allowText = false;

QueueHandle_t nStatQueueHandle;
QueueHandle_t cdcCtlQueueHandle;
QueueHandle_t wheelBtnQueueHandle;
BaseType_t nStatQueueResult;

BaseType_t wheelBtnQueueResult;

osThreadId nodeStatTaskHandle;
osThreadId cdcCtlTaskHandle;
osThreadId wheelBtnTaskHandle;
/* USER CODE END Variables */
osThreadId defaultTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
void wheelBtnConsumerTask(void const * argument);
void nodeStatusConsumerTask(void const * argument);
void cdcCtlTask(void const * argument);

void Beep(uint8_t type);
void requestTextOnDisplay(uint8_t type);
void writeTextOnDisplay();

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  osThreadDef(debugLedTask, StartDebugLedTask, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
  debugLedTaskHandle = osThreadCreate(osThread(debugLedTask), NULL);

  osThreadDef(replyTask, nodeStatusConsumerTask, osPriorityNormal, 0, 256);
  nodeStatTaskHandle = osThreadCreate(osThread(replyTask), NULL);

  osThreadDef(miscTask, cdcCtlTask, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
  cdcCtlTaskHandle = osThreadCreate(osThread(miscTask), NULL);

  osThreadDef(statusTask, wheelBtnConsumerTask, osPriorityNormal, 0, 256);
  wheelBtnTaskHandle = osThreadCreate(osThread(statusTask), NULL);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  nStatQueueHandle = xQueueCreate(8, sizeof(uint8_t));
  cdcCtlQueueHandle = xQueueCreate(8, sizeof(uint8_t));
  wheelBtnQueueHandle = xQueueCreate(8, sizeof(uint8_t));
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  uint8_t cdcActiveStatus[] = {0xE0,0xFF,0x3F,0x41,0xFF,0xFF,0xFF,0xD0};
  uint8_t cdcInactiveStatus[] = {0xE0,0xFF,0x3F,0x00,0x00,0x00,0x00,0xD0};
  uint8_t trionicVin[] = {0x00,0x00,0x00,0x00,0x90,0x56,0x01,0x00};
  uint8_t queueTest[5][8] = {
      {0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00},
      {0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x00},
      {0x00,0x00,0x00,0x08,0x00,0x00,0x00,0x00},
      {0x80,0x24,0x00,0x00,0x00,0x00,0x00,0x00},
      {0x00,0x00,0x10,0x00,0x00,0x00,0x00,0x00}
  };
//  uint8_t i = 0;
//  CAN_Send_Data(CDC_CONTROL, queueTest[3]);
  osDelay(15);
  for(;;)
  {
 /* Probe led status*/
//    huart2_send_byte(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1));

    SetDebugLed();
//    CAN_Send_Data(NODE_STATUS_RX_IHU, queueTest[3]);
//    CAN_Send_Data(NODE_STATUS_RX_IHU, queueTest[i++]);
//    i = i>2 ? 0:i;
//    osDelay(15);
//    CAN_Send_Data(VIN_STATUS_TRIONIC, trionicVin);

    osDelay(15);
    if (cdcActive){
      CAN_Send_Data(GENERAL_STATUS_CDC, cdcActiveStatus);
    } else{
      CAN_Send_Data(GENERAL_STATUS_CDC, cdcInactiveStatus);
    }
    osDelay(950);
//    CAN_Send_Data(STEERING_WHEEL_BUTTONS, queueTest[4]);
//    osDelay(5000);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void nodeStatusConsumerTask(void const * argument)
{
  uint8_t cdcPoweronCmd[4][8] = {
      {0x32,0x00,0x00,0x03,0x01,0x02,0x00,0x00},
      {0x42,0x00,0x00,0x22,0x00,0x00,0x00,0x00},
      {0x52,0x00,0x00,0x22,0x00,0x00,0x00,0x00},
      {0x62,0x00,0x00,0x22,0x00,0x00,0x00,0x00}
  };
  uint8_t cdcActiveCmd[4] [8] = {
      {0x32,0x00,0x00,0x16,0x01,0x02,0x00,0x00},
      {0x42,0x00,0x00,0x36,0x00,0x00,0x00,0x00},
      {0x52,0x00,0x00,0x36,0x00,0x00,0x00,0x00},
      {0x62,0x00,0x00,0x36,0x00,0x00,0x00,0x00},
  };
  uint8_t cdcPowerdownCmd[4] [8] = {
      {0x32,0x00,0x00,0x19,0x01,0x00,0x00,0x00},
      {0x42,0x00,0x00,0x38,0x01,0x00,0x00,0x00},
      {0x52,0x00,0x00,0x38,0x01,0x00,0x00,0x00},
      {0x62,0x00,0x00,0x38,0x01,0x00,0x00,0x00}
  };
  const TickType_t xTicksToWait = pdMS_TO_TICKS( 10000 );
  uint8_t ReceivedValue;
  BaseType_t xStatus;
  /* Infinite loop */
  for(;;)
  {
    xStatus = xQueueReceive(nStatQueueHandle, &ReceivedValue, xTicksToWait);
    if(xStatus == pdPASS){
      switch (ReceivedValue & 0x0F) {
        case 0x3:
//          huart2_send((uint8_t*)"PowerOn\n", 8);
          for (uint8_t i=0; i<4; i++)
          {
            CAN_Send_Data(NODE_STATUS_TX_CDC, cdcPoweronCmd[i]);
            osDelay(15);
          }
          break;
        case 0x2:
//          huart2_send((uint8_t*)"Active\n", 7);
          for (uint8_t i=0; i<4; i++)
          {
            CAN_Send_Data(NODE_STATUS_TX_CDC, cdcActiveCmd[i]);
            osDelay(15);
          }
          break;
        case 0x8:
//          huart2_send((uint8_t*)"PowerOff\n", 9);
          for (uint8_t i=0; i<4; i++)
          {
            CAN_Send_Data(NODE_STATUS_TX_CDC, cdcPowerdownCmd[i]);
            osDelay(15);
          }
          break;
        default:
          break;
      }
    }

  }
}

void cdcCtlTask(void const * argument)
{
  BaseType_t xStatus;
  uint8_t ReceivedValue;
  for(;;){
    xStatus = xQueueReceive(cdcCtlQueueHandle, &ReceivedValue, pdMS_TO_TICKS(100));
    if(xStatus == pdPASS){
//    huart2_send((uint8_t*)"CDC_CONSUMER\n", 13);
      switch (ReceivedValue) {
        case 0x24: //CDC = ON (CD/RDM button has been pressed twice)
//          huart2_send((uint8_t*)"CDC_ON!\n", 8);
//          xTaskResumeFromISR(cdcCtlTaskHandle);
          cdcActive = true;
          Power(cdcActive);
          break;
        case 0x14: // CDC = OFF (Back to Radio or Tape mode)
          cdcActive = false;
          Power(cdcActive);
          break;
        case 0x35: // Seek >>
          NextTrack();
          break;
        case 0x36: // Seek <<
          PrevTrack();
          break;
        default:
          break;
      }
//      if(cdcActive){
//        osDelay(15);
//        requestTextOnDisplay(0x01);
//        osDelay(100);
//        if (allowText){
//          writeTextOnDisplay();
//          allowText = false;
//        }
//        osDelay(950);
//      }
    }
//    vTaskSuspend(NULL);
//    ulTaskNotifyTake(pdTRUE,pdMS_TO_TICKS(1000));
//    vTaskSuspend(NULL);
  }
}

void wheelBtnConsumerTask(void const * argument)
{
  BaseType_t xStatus;
  uint8_t ReceivedValue;
  for(;;)
  {
    xStatus = xQueueReceive(nStatQueueHandle, &ReceivedValue, pdMS_TO_TICKS(10000));
    if(xStatus == pdPASS){
      switch (ReceivedValue) {
        case 0x04: // NXT button on wheel
          PlayPause();
          break;
        case 0x10: // Seek>> button on wheel
          NextTrack();
          break;
        case 0x08: // Seek<< button on wheel
          PrevTrack();
          break;
        default:
          break;
    }
    }
  }
}

void Beep(uint8_t type){
  //0x04    Short "Beep"
  //0x08    "Tack"
  //0x10    "Tick"
  //0x40    Short "Ding-Dong"
  static uint8_t msg_beep[] = {0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  msg_beep[1] = type;
  CAN_Send_Data(0x430, msg_beep);
}

void requestTextOnDisplay(uint8_t type){
  /* Format of NODE_DISPLAY_RESOURCE_REQ frame:
   ID: Node ID requesting to write on SID
   [0]: Request source
   [1]: SID object to write on; 0 = entire SID; 1 = 1st row; 2 = 2nd row
   [2]: Request type: 1 = Engineering test; 2 = Emergency; 3 = Driver action; 4 = ECU action; 5 = Static text; 0xFF = We don't want to write on SID
   [3]: Request source function ID
   [4-7]: Zeroed out; not in use
   */

  uint8_t displayRequestCmd[8];

  displayRequestCmd[0] = NODE_APL_ADR;
  displayRequestCmd[1] = DESIRED_ROW & (0x0F);
  displayRequestCmd[2] = type;
  displayRequestCmd[3] = NODE_SID_FUNCTION_ID;
  displayRequestCmd[4] = 0x00;
  displayRequestCmd[5] = 0x00;
  displayRequestCmd[6] = 0x00;
  displayRequestCmd[7] = 0x00;
//  huart2_send((uint8_t*)"req\n", 4);
  CAN_Send_Data(NODE_DISPLAY_RESOURCE_REQ, displayRequestCmd);
}

void writeTextOnDisplay(){
  uint8_t SIDtext[3][8] = {
      {0x42,0x96,DESIRED_ROW,'B','l','u','e','t'},
      {0x01,0x96,DESIRED_ROW,'o','o','t','h',0x20},
      {0x00,0x96,DESIRED_ROW,0x20,0x20,0x00,0x00,0x00}
  };

  for (uint8_t k=0;k<3;k++){
    CAN_Send_Data(NODE_WRITE_TEXT_ON_DISPLAY, SIDtext[k]);
    osDelay(10);
  }

}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
