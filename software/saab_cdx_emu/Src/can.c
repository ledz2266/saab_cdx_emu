/**
  ******************************************************************************
  * File Name          : CAN.c
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

#include "gpio.h"

/* USER CODE BEGIN 0 */
#include <string.h>
#include <stdbool.h>
#include "UART.h"
#include "CDC.h"

//#define ENABLE_HUART2

//extern osThreadId cdcCtlTaskHandle;
//extern osThreadId nodeStatTaskHandle;
//extern osThreadId wheelBtnTaskHandle;

extern QueueHandle_t nStatQueueHandle;
extern QueueHandle_t cdcCtlQueueHandle;
extern QueueHandle_t wheelBtnQueueHandle;
//extern BaseType_t cdcCtlQueueResult;
//extern BaseType_t nStatQueueResult;
//extern BaseType_t wheelBtnQueueResult;

extern bool allowText;

CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
uint8_t               TxData[8];
uint8_t               RxData[8];
uint32_t              TxMailbox;

/* USER CODE END 0 */

CAN_HandleTypeDef hcan;

/* CAN init function */
void MX_CAN_Init(void)
{

  hcan.Instance = CAN;
  hcan.Init.Prescaler = 36;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_15TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_5TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(canHandle->Instance==CAN)
  {
  /* USER CODE BEGIN CAN_MspInit 0 */

  /* USER CODE END CAN_MspInit 0 */
    /* CAN clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();
  
    /**CAN GPIO Configuration    
    PA11     ------> CAN_RX
    PA12     ------> CAN_TX 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_CAN;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* CAN interrupt Init */
    HAL_NVIC_SetPriority(CAN_TX_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN_TX_IRQn);
    HAL_NVIC_SetPriority(CAN_RX0_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN_RX0_IRQn);
    HAL_NVIC_SetPriority(CAN_RX1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(CAN_RX1_IRQn);
  /* USER CODE BEGIN CAN_MspInit 1 */

  /* USER CODE END CAN_MspInit 1 */
  }
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN)
  {
  /* USER CODE BEGIN CAN_MspDeInit 0 */

  /* USER CODE END CAN_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();
  
    /**CAN GPIO Configuration    
    PA11     ------> CAN_RX
    PA12     ------> CAN_TX 
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* CAN interrupt Deinit */
    HAL_NVIC_DisableIRQ(CAN_TX_IRQn);
    HAL_NVIC_DisableIRQ(CAN_RX0_IRQn);
    HAL_NVIC_DisableIRQ(CAN_RX1_IRQn);
  /* USER CODE BEGIN CAN_MspDeInit 1 */
    HAL_NVIC_EnableIRQ(CAN_RX0_IRQn);
    HAL_NVIC_EnableIRQ(CAN_RX1_IRQn);
  /* USER CODE END CAN_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
void CAN_User_Config(void){
  MX_CAN_Init();

  /*##-2- Configure the CAN Filter ###########################################*/

  CAN_FilterTypeDef  sFilterConfig;
  CAN_FilterTypeDef  sFilter1Config;
  CAN_FilterTypeDef  sFilter2Config;

  sFilterConfig.FilterBank = 0;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = 0x0368<<5;
  sFilterConfig.FilterIdLow = 0x0000;
//  sFilterConfig.FilterMaskIdHigh = 0xFF00<<5;
  sFilterConfig.FilterMaskIdHigh = 0xFFFF<<5;
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = 14;

  if (HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
  {
    /* Filter configuration Error */
    Error_Handler();
  }

  sFilter1Config.FilterBank = 1;
  sFilter1Config.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilter1Config.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilter1Config.FilterIdHigh = 0x03C0<<5;
  sFilter1Config.FilterIdLow = 0x0000;
  sFilter1Config.FilterMaskIdHigh = 0xFFFF<<5;
  sFilter1Config.FilterMaskIdLow = 0x0000;
  sFilter1Config.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilter1Config.FilterActivation = ENABLE;
  sFilter1Config.SlaveStartFilterBank = 14;

  if (HAL_CAN_ConfigFilter(&hcan, &sFilter1Config) != HAL_OK)
  {
    /* Filter configuration Error */
    Error_Handler();
  }

  sFilter2Config.FilterBank = 2;
  sFilter2Config.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilter2Config.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilter2Config.FilterIdHigh = 0x06A1<<5;
  sFilter2Config.FilterIdLow = 0x0000;
  sFilter2Config.FilterMaskIdHigh = 0xFFFF<<5;
  sFilter2Config.FilterMaskIdLow = 0x0000;
  sFilter2Config.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilter2Config.FilterActivation = ENABLE;
  sFilter2Config.SlaveStartFilterBank = 14;

  if (HAL_CAN_ConfigFilter(&hcan, &sFilter2Config) != HAL_OK)
  {
    /* Filter configuration Error */
    Error_Handler();
  }

  /*##-3- Start the CAN peripheral ###########################################*/
  if (HAL_CAN_Start(&hcan) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }

  /*##-4- Activate CAN RX notification #######################################*/
  if (HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
  {
    /* Notification Error */
    Error_Handler();
  }

  if (HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO1_MSG_PENDING) != HAL_OK)
  {
    /* Notification Error */
//    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1);
    Error_Handler();
  }

  /*##-5- Configure Transmission process #####################################*/
  TxHeader.StdId = 0x321;
  TxHeader.ExtId = 0x00;
  TxHeader.RTR = CAN_RTR_DATA;
  TxHeader.IDE = CAN_ID_STD;
  TxHeader.DLC = 8;
  TxHeader.TransmitGlobalTime = DISABLE;
}

void CAN_Send_Data(uint32_t ID, uint8_t data[8]){
  HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_4);
  TxHeader.StdId = ID;
//  TxHeader.RTR = CAN_RTR_DATA;
//  TxHeader.IDE = ID;
//  TxHeader.DLC = 8;
#ifdef ENABLE_HUART2
  huart2_send((uint8_t*)"TX>",3);
  huart2_send_CANID(TxHeader.StdId);
  huart2_send_char(' ');
  huart2_send_CANDATA(data);
  huart2_send_char('\n');
#endif

  if (HAL_CAN_AddTxMessage(&hcan, &TxHeader, data, &TxMailbox) != HAL_OK){
      /* Transmission request Error */
      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, 1);
      Error_Handler();
    }
//    HAL_Delay(15);
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
  /* Get RX message */
  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
  {
    /* Reception Error */
    Error_Handler();
  }
#ifdef ENABLE_HUART2
  huart2_send((uint8_t*)"RX<",3);
  huart2_send_CANID(RxHeader.StdId);
  huart2_send_char(' ');
  huart2_send_CANDATA(RxData);
  huart2_send_char('\n');
#endif
  switch (RxHeader.StdId) {
    case NODE_STATUS_RX_IHU:
//      HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0);
      xQueueSendToBackFromISR(nStatQueueHandle, &RxData[3], 0);
      break;
    case CDC_CONTROL:
      if(RxData[0] == 0x80){
        xQueueSendToBackFromISR(cdcCtlQueueHandle, &RxData[1], 0);
      }
      break;
    case STEERING_WHEEL_BUTTONS:
      xQueueSendToBackFromISR(wheelBtnQueueHandle, &RxData[2], 0);
      break;
    case DISPLAY_RESOURCE_GRANT:
      if ((RxData[0] == (DESIRED_ROW & 0x0F)) && (RxData[1] == NODE_SID_FUNCTION_ID) && (!allowText)){
//        huart2_send_char('t');
        allowText = true;
      }
      break;
    default:
      break;
  }
//  vTaskNotifyGiveFromISR(replyTaskHandle, NULL);
//  xTaskResumeFromISR(replyTaskHandle);
//  xTaskResumeFromISR(miscTaskHandle);
//  if ((RxData[0] == (DESIRED_ROW & 0x0F)) && (RxData[1] == NODE_SID_FUNCTION_ID) && (!allowText)){
//    // Write permission granted
////    allowText = true;
//    huart2_send((uint8_t*)"go\n",3);
//    xTaskResumeFromISR(miscTaskHandle);
////    vTaskNotifyGiveFromISR(miscTaskHandle, NULL);
//  }

}
/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
