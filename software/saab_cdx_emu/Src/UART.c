/*
 * UART.c
 *
 *  Created on: 11 Feb 2019
 *      Author: dels
 */
#include "UART.h"

void huart2_send(uint8_t TxBuffer[], uint8_t len){
  if (HAL_UART_Transmit(&huart2, (uint8_t*)TxBuffer, len, 5000)!= HAL_OK)
    {
      Error_Handler();
    }
}

void huart2_send_byte(uint8_t TxByte){
  char str_buf[3];
  if (HAL_UART_Transmit(&huart2, (uint8_t*)str_buf, sprintf(str_buf, "%02X", TxByte), 5000)!= HAL_OK)
    {
      Error_Handler();
    }
}

void huart2_send_char(char TxChar){
  if (HAL_UART_Transmit(&huart2, (uint8_t*)&TxChar, 1, 5000)!= HAL_OK)
    {
      Error_Handler();
    }
}

void huart2_send_CANID(uint32_t TxBytes){
  char str_buf[5];
  if (HAL_UART_Transmit(&huart2, (uint8_t*)str_buf, sprintf(str_buf, "%04X", TxBytes), 5000)!= HAL_OK)
    {
      Error_Handler();
    }
}

void huart2_send_CANDATA(uint8_t TxBuffer[8]){
  for(uint8_t j=0;j<8;j++){
    huart2_send_char(' ');
    huart2_send_byte(TxBuffer[j]);
  }
}
