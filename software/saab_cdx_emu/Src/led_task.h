#ifndef __LED_H
#define __LED_H

#include "main.h"
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

void SetDebugLed(void);
void StartDebugLedTask(void const * argument);

osThreadId debugLedTaskHandle;

//osThreadDef(debugLedTask, DebugLedTask, osPriorityNormal, 0, 128);
//debugLedTaskHandle = osThreadCreate(osThread(debugLedTask), NULL);

#endif /* __LED_H */
