################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/UART.c \
../Src/can.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/led_task.c \
../Src/main.c \
../Src/stm32f3xx_hal_msp.c \
../Src/stm32f3xx_hal_timebase_TIM.c \
../Src/stm32f3xx_it.c \
../Src/system_stm32f3xx.c \
../Src/usart.c 

OBJS += \
./Src/UART.o \
./Src/can.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/led_task.o \
./Src/main.o \
./Src/stm32f3xx_hal_msp.o \
./Src/stm32f3xx_hal_timebase_TIM.o \
./Src/stm32f3xx_it.o \
./Src/system_stm32f3xx.o \
./Src/usart.o 

C_DEPS += \
./Src/UART.d \
./Src/can.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/led_task.d \
./Src/main.d \
./Src/stm32f3xx_hal_msp.d \
./Src/stm32f3xx_hal_timebase_TIM.d \
./Src/stm32f3xx_it.d \
./Src/system_stm32f3xx.d \
./Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F303x8 -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Inc" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Drivers/STM32F3xx_HAL_Driver/Inc" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Drivers/STM32F3xx_HAL_Driver/Inc/Legacy" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Middlewares/Third_Party/FreeRTOS/Source/include" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"/home/dels/workspace/hw/saab_cdx_emu/software/saab_cdx_emu1101/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


